#!/usr/bin/python3

import os
import json
import shutil
import sys
import subprocess

from collections import defaultdict
from datetime import datetime


def check_and_clean(indict):
    retdict = defaultdict(dict)

    # 100Mbs Warn | 50Mb/s Crit
    if indict['download'] < 100000000:
        if indict['download'] < 50000000:
            pcolor = 'red'
        else:
            pcolor = 'orange'
    else:
        pcolor = 'black'
    
    # Format it as Mb/s with 3 decimal points
    retdict['download']['color'] = pcolor
    retdict['download']['value'] = "{:0.3f} Mb/s".format(float(indict['download']/1000000))

    # 10Mbs Warn | 7.5Mbs Crit
    if indict['upload'] < 10000000:
        if indict['upload'] < 7500000:
            pcolor = 'red'
        else:
            pcolor = 'orange'
    else:
        pcolor = 'black'

    retdict['upload']['color'] = pcolor
    retdict['upload']['value'] = "{:0.3f} Mb/s".format(float(indict['upload']/1000000))

    # Start at 100ms
    if indict['ping'] > 50:
        if indict['ping'] > 100:
            pcolor = 'red'
        else:
            pcolor = 'orange'
    else:
        pcolor = 'black'

    retdict['ping']['color'] = pcolor
    retdict['ping']['value'] = "{:0.3f} ms".format(indict['ping'])

    retdict['location'] = indict['server']['name']
    retdict['distance'] = "{:0.3f} km".format(indict['server']['d'])

    # TODO: Have a check to see if this changed... 
    retdict['ip'] = indict['client']['ip']


    return retdict


def currpage_build(indict):
    htmstring = "<!DOCTYPE html>\n<html>\n<body>\n\n<p>\n"

    # Set up the header and footer for the iframe html
    htmstring += 'Last run at: ' + str(CURTIME) + ' <br>\n'
    # TODO: This might also be red...
    htmstring += 'My IP: ' + indict['ip'] + ' <br>\n'
    htmstring +=  'Server Location: ' + indict['location'] +\
         ' Distance: ' + indict['distance'] + ' <br>\n'

    htmstring += '<span style="color:' + indict['ping']['color'] + '">Ping: ' + indict['ping']['value'] + ' </span><br>\n' 

    htmstring += '<span style="color:' + indict['download']['color']  + '">Download: ' + indict['download']['value'] + ' </span><br>\n'
    htmstring += '<span style="color:' + indict['upload']['color']  + '">Upload: ' + indict['upload']['value'] + ' </span><br>\n'

    htmstring += '</p>\n\n</body>\n</html>'
    
    iframefile = '/var/www/html/lastspeedrun.html'
    tmpfile = '/tmp/' + FILETIME + '.html'
    
    try:
        with open(tmpfile, 'w') as t:
            t.write(htmstring)

        shutil.copyfile(tmpfile, iframefile)
    except Exception as e:
        print("Failed to write the current html.")
        print(e)
    finally:
        os.remove(tmpfile)


    return 


def histpage_build(indict):
    # This is for the daily history file
    ftime = CURTIME.strftime('%F')

    histstring = '<tr>\n'
    histstring += '<td width="200px">' + str(CURTIME) + '</td>\n'
    histstring += '<td width="150px">' + indict['ip'] + '</td>\n'
    histstring += '<td width="100px">' + indict['location'] + '</td>\n'
    histstring += '<td width="100px">' + indict['distance'] + '</td>\n'
    histstring += '<td style="width:100px;color:' + indict['ping']['color'] + '">' + indict['ping']['value'] + '</td>\n' 
    histstring += '<td style="width:100px;color:' + indict['download']['color'] + '">' + indict['download']['value'] + '</td>\n' 
    histstring += '<td style="width:100px;color:' + indict['upload']['color'] + '">' + indict['upload']['value'] + '</td>\n' 
    histstring += '</tr>\n'

    curhistfile = '/var/www/html/history/' + ftime + '.html'
    
    # If the history file for today exists load it.
    # If not use the template file.
    if os.path.isfile(curhistfile):
        loadfile = curhistfile
    else:
        loadfile = '/var/www/html/history/template.html'

    with open(loadfile) as r:
        htmlist = r.readlines()
    
    # Find the "End Table" line and put our new string just before that
    etindex = htmlist.index('<!-- End Table -->\n')

    htmlist.insert(etindex, histstring)

    htmwrite = ''.join(htmlist)
    
    try:
        with open(curhistfile, 'w') as hf:
            hf.write(htmwrite)
    except Exception as e:
        print("Could not write histfile")
        print (e)
    
    return 


def log_notible_events(indict):
    checklist = ['ping', 'download', 'upload']
    
    textlogs = []
    htmlogs = []

    for c in checklist:
        if indict[c]['color'] == 'black':
            continue

        # First for our plaintext log
        logtext = 'Date: '+ str(CURTIME) + ' Component: ' + c + ' Level: ' + indict[c]['color'] + ' Value: ' + indict[c]['value'] + '\n'

        textlogs.append(logtext)
        
        htmtext = '<span style="color:' + indict[c]['color'] + '">' + str(CURTIME) + ' | ' + c + ': ' + indict[c]['value'] + '</span><br>\n'
        htmlogs.append(htmtext)
    
    # If there are events in the list update the logs
    if len(textlogs) > 0:
        logstring = ''.join(textlogs)
        # Just append to the text log
        with open('/var/www/html/full_evs.log', 'a') as fl:
            fl.write(logstring)

        # This is a list of the last 20 events that is displayed in an iframe
        with open('/var/www/html/last20evs.html') as lt:
            htmlist = lt.readlines()
    
        # Just get the data between the start and end
        sindex = (htmlist.index('<!-- Start Logs -->\n') + 1)
        findex = htmlist.index('<!-- End Logs -->\n')
        datalist = htmlist[sindex:findex]
    
        for h in htmlogs:
            datalist.append(h)
    
        # Limit this to the last 20 events
        last_twendy = ''.join(datalist[-20:])

        htmstring = "<!DOCTYPE html>\n<html>\n<body>\n\n<!-- Start Logs -->\n" + last_twendy
        htmstring += '<!-- End Logs -->\n\n</body>\n</html>'
        
        with open('/var/www/html/last20evs.html', 'w') as ltw:
            ltw.write(htmstring)
    
    return


if __name__ == '__main__':
    # Get the current time.  
    # The filetime is formatted in epoch
    CURTIME = datetime.now()
    FILETIME = CURTIME.strftime('%s')

    # Popen the command... and get the output into a list... 
    proc = subprocess.Popen(['/usr/bin/speedtest-cli', '--json'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Give up after 90 seconds
    spout = proc.communicate(timeout=90)[0].decode('utf-8').strip()
    
    # Write the raw JSON to a file for later use
    jsonhistfile = '/var/www/html/history/json/' + FILETIME + '.json'
    with open(jsonhistfile, 'w') as j:
        json.dump(spout, j)

    # Convert from json to Python dict
    sdict = json.loads(spout)
    
    # Clean up and check the data
    cleaneddict = check_and_clean(sdict)
    
    # Use the cleaned data to build the current page
    # add the info to a history page for the day
    # and if there are Orange/Red events log those
    currpage_build(cleaneddict)
    histpage_build(cleaneddict)
    log_notible_events(cleaneddict)

    sys.exit(0)